<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

// S
	'stopoclicdroit_description' => 'Ce plugin permet de bloquer les clics droits, pratique pour
	des bornes multimedias branchées sur un site web',
	'stopoclicdroit_slogan' => 'Bloque les clics droits',
);
?>
